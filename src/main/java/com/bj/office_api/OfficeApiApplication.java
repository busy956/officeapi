package com.bj.office_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OfficeApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OfficeApiApplication.class, args);
	}

}
