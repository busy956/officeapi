package com.bj.office_api.controller;

import com.bj.office_api.model.OfficeItem;
import com.bj.office_api.model.OfficeRequest;
import com.bj.office_api.model.OfficeResponse;
import com.bj.office_api.service.OfficeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/office")
public class OfficeController {
    private final OfficeService officeService;

    @PostMapping("/man")
    public String setOffice(@RequestBody OfficeRequest request) {
        officeService.setOffice(request);

        return "OK";
    }
    @GetMapping("/all")
    public List<OfficeItem> getOffices() {
        return officeService.getOffices();
    }

    @GetMapping("/detail/{id}")
    public OfficeResponse getOffice(@PathVariable long id) {
        return officeService.getOffice(id);
    }
}
