package com.bj.office_api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OfficePosition {
    SAWON("사원"),
    DERIE("대리"),
    GUAJANG("과장"),
    CHAJANG("차장"),
    BUJANG("부장");

    private final String name;
}
