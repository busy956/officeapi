package com.bj.office_api.service;

import com.bj.office_api.entity.Office;
import com.bj.office_api.model.OfficeItem;
import com.bj.office_api.model.OfficeRequest;
import com.bj.office_api.model.OfficeResponse;
import com.bj.office_api.repository.OfficeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OfficeService {
    private final OfficeRepository officeRepository;

    public void setOffice(OfficeRequest request) {
        Office addData = new Office();
        addData.setJoinDate(request.getJoinDate());
        addData.setEmployeeName(request.getEmployeeName());
        addData.setPosition(request.getPosition());
        addData.setDepartment(request.getDepartment());
        addData.setRegularStatus(request.getRegularStatus());
        addData.setEtcMemo(request.getEtcMemo());

        officeRepository.save(addData);
    }

    public List<OfficeItem> getOffices() {
        List<Office> originData = officeRepository.findAll();

        List<OfficeItem> result = new LinkedList<>();

        for (Office office : originData) {
            OfficeItem addItem = new OfficeItem();
            addItem.setId(office.getId());
            addItem.setJoinDate(office.getJoinDate());
            addItem.setEmployeeName(office.getEmployeeName());
            addItem.setPosition(office.getPosition().getName());
            addItem.setDepartment(office.getDepartment());

            result.add(addItem);
        }
        return result;
    }

    public OfficeResponse getOffice(long id) {
        Office originData = officeRepository.findById(id).orElseThrow();

        OfficeResponse response = new OfficeResponse();
        response.setId(originData.getId());
        response.setJoinDate(originData.getJoinDate());
        response.setEmployeeName(originData.getEmployeeName());
        response.setPosition(originData.getPosition().getName());
        response.setDepartment(originData.getDepartment());
        response.setRegularStatus(originData.getRegularStatus() ? "예" : "아니오");
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }
}
