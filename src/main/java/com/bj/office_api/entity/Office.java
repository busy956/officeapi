package com.bj.office_api.entity;

import com.bj.office_api.enums.OfficePosition;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Office {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate joinDate;

    @Column(nullable = false)
    private String employeeName;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private OfficePosition position;

    @Column(nullable = false)
    private String department;

    @Column(nullable = false)
    private Boolean regularStatus;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
