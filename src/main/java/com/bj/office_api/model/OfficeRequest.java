package com.bj.office_api.model;

import com.bj.office_api.enums.OfficePosition;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class OfficeRequest {
    private LocalDate joinDate;

    private String employeeName;

    @Enumerated(value = EnumType.STRING)
    private OfficePosition position;

    private String department;

    private Boolean regularStatus;

    private String etcMemo;


}
