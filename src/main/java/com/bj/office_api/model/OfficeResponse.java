package com.bj.office_api.model;

import com.bj.office_api.enums.OfficePosition;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class OfficeResponse {
    private Long id;
    private LocalDate joinDate;
    private String employeeName;
    private String position;
    private String department;
    private String regularStatus;
    private String etcMemo;
}
